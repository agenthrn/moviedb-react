import React from "react";

function MovieCard(props) {
  return (
    <div className="movie_card">
      <img
        src={`https://image.tmdb.org/t/p/w200/${props.item.poster_path}`}
        alt={props.item.title}
      />
      <p>{props.item.title}</p>
    </div>
  );
}

export default MovieCard;
