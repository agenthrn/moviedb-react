import { combineReducers } from "redux";
import listMovieReducer from "./movie";
export default combineReducers({
  list: listMovieReducer,
});
