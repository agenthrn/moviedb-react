import { GET_MOVIES } from "../types";

const initialState = {
  movie: [],
  loading: true,
};
const listMovieReducer = (state = initialState, action) => {
  const { type, payload } = action;
  switch (type) {
    case GET_MOVIES:
      return {
        ...state,
        movie: payload,
        loading: false,
      };
    default:
      return state;
  }
};

export default listMovieReducer;
