// import logo from './logo.svg';
import { Provider } from "react-redux";
import "./App.scss";
import Header from "./components/Header";
import Popular from "./components/Popular";
import store from "./store";

function App() {
  return (
    <Provider store={store}>
      <div id="app">
        <Header />
        <Popular />
      </div>
    </Provider>
  );
}

export default App;
