import React from "react";

function Header() {
  return (
    <div id="header_section">
      <h1>MovieDB</h1>
      <p>Tempat cari info favorit film anda</p>
    </div>
  );
}

export default Header;
