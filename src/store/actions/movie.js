import { GET_MOVIES, MOVIES_ERROR } from "../types";
import axios from "axios";

export const getPopular = () => async (dispatch) => {
  try {
    const res =
      await axios.get(`https://api.themoviedb.org/3/movie/popular?api_key=29f7f255943cd94fc93c276073de6afc&language=en-US&page=1
    `);
    dispatch({
      type: GET_MOVIES,
      payload: res.data.results,
    });
  } catch (e) {
    dispatch({
      type: MOVIES_ERROR,
      payload: console.log(e),
    });
  }
};
