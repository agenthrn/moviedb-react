import React, { useEffect, useState } from "react";
import MovieCard from "./MovieCard";
import { connect } from "react-redux";
import { getPopular } from "../store/actions/movie";

function Popular(props) {
  useEffect(() => {
    props.getPopular();
  }, []);

  return (
    <div id="popular">
      <p>Popular</p>
      <ul>
        {props.movie &&
          props.movie.map((item, index) => (
            <li key={index}>
              <MovieCard item={item} />
            </li>
          ))}
      </ul>
    </div>
  );
}

const mapStateToProps = (state) => {
  return { movie: state.list.movie };
};

export default connect(mapStateToProps, { getPopular })(Popular);
